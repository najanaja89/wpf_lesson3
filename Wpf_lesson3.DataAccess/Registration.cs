﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wpf_lesson3.Models;

namespace Wpf_lesson3.DataAccess
{
    public class Registration
    {
        User User { get; set; }

        public Registration()
        {
            User user = new User
            {
                Login = "",
                Password = "",
            };

            User = user;
        }

        public bool UserExist(string login)
        {
            using (var context = new SecurityContext())
            {
                var isExist = context.Users.Any(user => user.Login == login);
                return isExist;
            }
        }

        public void RegisterUser(User user)
        {
            using (var context = new SecurityContext())
            {
                context.Users.Add(user);
                context.SaveChanges();
            }
        }

        public User ValidtionCheck(string password, string reTypePassword, string login)
        {
            var hasNumber = new Regex(@"[0-9]+");
            var hasUpperChar = new Regex(@"[A-Z]+");
            var hasMinimum8Chars = new Regex(@".{8,}");

            User.Login = login;
            User.Password = password;

            var isValidated = hasNumber.IsMatch(User.Password) && hasUpperChar.IsMatch(User.Password) && hasMinimum8Chars.IsMatch(User.Password);
            if (isValidated)
            {
                string doublePassword = reTypePassword;
                if (doublePassword != User.Password)
                {
                    MessageBox.Show("Passwords did not match!");
                    return null;
                }
                else return User;
            }
            else
            {
                MessageBox.Show("Password must contain minimum 8 symbols, one upper case letter, one digit");
                return null;
            }

        }
    }

}
