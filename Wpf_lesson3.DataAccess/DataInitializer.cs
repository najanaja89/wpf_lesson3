﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Wpf_lesson3.Models;
using Wpf_lesson3.Services;

namespace Wpf_lesson3.DataAccess
{
    public class DataInitializer:DropCreateDatabaseAlways<SecurityContext>
    {
        protected override void Seed(SecurityContext context)
        {
            context.Users.Add(new User
            {
                Login = "admin",
                Password = DataEcnryptor.HashPassword("root")
            });
        }
    }
}
