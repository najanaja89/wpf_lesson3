﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Wpf_lesson3.DataAccess;
using Wpf_lesson3.Services;

namespace Wpf_lesson3
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void SignInButtonClick(object sender, RoutedEventArgs e)
        {
            var login = loginTextBox.Text;
            var password = passwordTextBox.Password;

            if (string.IsNullOrEmpty(login) || string.IsNullOrEmpty(password))
            {
                MessageBox.Show("Enter All Fields");
                return;
            }

            using (var context = new SecurityContext())
            {
                var user = context.Users.FirstOrDefault(searchingUser => searchingUser.Login == login);

                if (user != null && DataEcnryptor.IsValidPassword(password, user.Password))
                {
                    MessageBox.Show("Welcome!");
                }
                else
                {
                    MessageBox.Show("Fuck Off!");
                }
            }
        }

        private void SignUpButtonClick(object sender, RoutedEventArgs e)
        {
            RegistrationWindow registrationWindow = new RegistrationWindow();
            registrationWindow.Show();
        }
    }
}
