﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Wpf_lesson3.DataAccess;
using Wpf_lesson3.Services;

namespace Wpf_lesson3
{
    /// <summary>
    /// Логика взаимодействия для RegistrationWindow.xaml
    /// </summary>
    public partial class RegistrationWindow : Window
    {
        public RegistrationWindow()
        {
            InitializeComponent();
        }

        private void RegisterButtonClick(object sender, RoutedEventArgs e)
        {
;
            var registration = new Registration();
            if (registration.UserExist(loginRegisterTextBox.Text.ToLower()))
            {
                MessageBox.Show("User Exist");
                return;
            }

            var user = registration.ValidtionCheck(passwordRegisterTextBox.Password, retypePasswordRegisterTextBox.Password, loginRegisterTextBox.Text.ToLower());
            if (user != null)
            {
                user.Password = DataEcnryptor.HashPassword(retypePasswordRegisterTextBox.Password);
                registration.RegisterUser(user);
                MessageBox.Show("Registration Successful");
            }
            else return;
            
        }

    }
}

